const icon = document.querySelectorAll('i')
const btn = document.querySelector('button')
const form = document.forms[0]
const inputTop = document.getElementById('input-top')
const inputBottom = document.getElementById('input-bottom')
const error = document.createElement('p')
const bottomLabel = document.querySelectorAll('.input-wrapper')[1]

form.addEventListener('click', (e) => {
    e.preventDefault();
    
    const target = e.target
    icon.forEach(item => {
        if (target === item) {
            item.classList.toggle('fa-eye-slash')
        if (item.previousElementSibling.getAttribute('type') === 'password'){
            item.previousElementSibling.setAttribute('type', 'text');
        } else {
            item.previousElementSibling.setAttribute('type', 'password')
        }}
    })
    
    if (target === btn) {
        if (inputTop.value === inputBottom.value) {
            alert('You are welcome')
        } else {
            error.textContent = 'Потрібно ввести однакові значення'
            error.style.color = 'red'
            bottomLabel.append(error)
        }
    }
})